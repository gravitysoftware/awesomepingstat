#!/bin/bash
trap exit SIGHUP SIGINT SIGQUIT SIGABRT

if [[ -z "$PS_DATADIR" ]]; then
  echo "Missing PS_DATADIR env var (csv files destination), using /data"
  PS_DATADIR="/data"
else
  echo "Using PS_DATADIR=$PS_DATADIR"
fi

if [[ -z "$PS_IP" ]]; then
  echo "Missing PS_IP env var (ping destination), using 1.1.1.1"
  PS_IP="1.1.1.1"
else
  echo "Using PS_IP=$PS_IP"
fi

if [[ -z "$PS_DURATION" ]]; then
  echo "Missing PS_DURATION env var (ping duration batch), using 60 (seconds)"
  PS_DURATION=60
else
  echo "Using PS_DURATION=$PS_DURATION"
fi

if [[ -z "$PS_COUNT" ]]; then
  echo "Missing PS_COUNT env var (ping count / ping duration), using $PS_DURATION"
  PS_COUNT=$PS_DURATION
else
  echo "Using PS_COUNT=$PS_COUNT"
fi

INTERVAL=$(echo $PS_DURATION $PS_COUNT | awk '{print $1/$2}')

function PING() {
  D=$(date +%Y%m%d)
  T=$(date +%H:%M:%S)
  P=$(ping -q -c$PS_COUNT -i$INTERVAL -w$PS_DURATION -W$INTERVAL -s16 -n $PS_IP | awk '/packet loss/{print $1 "," $4}')
  echo $D,$T,$PS_IP,$P
}

function main() {
  mkdir -pv "$PS_DATADIR/$PS_IP"
  if [[ $? -gt 0 ]];then
    exit 1
  fi
  while true; do
    F=$(date +"$PS_DATADIR/$PS_IP/%Y%m%d.csv")
    if [[ ! -s "$F" ]]; then
      echo "Create $F"
      echo "date,time,ip,transmitted,received" > "$F"
    fi
    PING >> "$F"
  done
}

main

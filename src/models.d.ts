interface IResSuccess<T> {
  data: T;
  error: null;
}
interface IResError {
  data: null;
  error: string;
}

type IRes<T> = IResSuccess<T> | IResError;

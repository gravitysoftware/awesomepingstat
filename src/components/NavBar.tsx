import { Button } from "./button";
import { ArrowLeftCircleIcon } from "@heroicons/react/16/solid";

export function NavBar(props: { href: string }) {
  return (
    <div className="h-10 flex p-4 bg-neutral-700 mb-4 items-center absolute top-0 inset-x-0 z-20">
      <Button plain href={props.href}>
        <ArrowLeftCircleIcon /> Go back
      </Button>
    </div>
  );
}

import { readdir } from "node:fs/promises";
import { Dirent } from "node:fs";
import { Link } from "@/components/link";
import { PS_DATADIR } from "@/settings";
import { NavBar } from "@/components/NavBar";

export const dynamic = "force-dynamic";

interface IStatFile {
  name: string;
  date: string;
}

class StatFile implements IStatFile {
  constructor(private _file: Dirent) {}

  get name() {
    return this._file.name;
  }

  get date() {
    return this._file.name.split(".")[0];
  }
}

async function getFiles(ip: string): Promise<IRes<IStatFile[]>> {
  try {
    const d = await readdir(`${PS_DATADIR}/${ip}`, {
      encoding: "utf-8",
      recursive: false,
      withFileTypes: true,
    });
    return {
      data: d
        .filter((file) => file.name.endsWith(".csv"))
        .map((file) => new StatFile(file)),
      error: null,
    };
  } catch (e) {
    return { data: null, error: e?.toString() ?? "unknown error" };
  }
}

export default async function Page({
  params: { ip },
}: {
  params: { ip: string };
}) {
  const files = await getFiles(ip);
  return (
    <main className="flex min-h-screen flex-col items-center justify-between p-24">
      <NavBar href="/" />
      {files.error != null ? (
        files.error
      ) : (
        <ul>
          {files.data.reverse().map((file) => (
            <li className="m-4" key={file.name}>
              <Link href={`/${ip}/${file.date}`}>{file.date}</Link>
            </li>
          ))}
        </ul>
      )}
    </main>
  );
}

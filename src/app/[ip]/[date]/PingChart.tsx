"use client";

import {
  Area,
  AreaChart,
  Line,
  LineChart,
  ResponsiveContainer,
  XAxis,
  YAxis,
} from "recharts";

interface IPingChartProps {
  rows: Record<string, string>[];
}

export function PingChart(props: IPingChartProps) {
  const { rows } = props;
  return (
    <ResponsiveContainer aspect={2}>
      <AreaChart data={rows}>
        <defs>
          <linearGradient id="colorUv" x1="0" y1="0" x2="0" y2="1">
            <stop offset="5%" stopColor="#EEEEEE" stopOpacity={0.8} />
            <stop offset="95%" stopColor="#EEEEEE" stopOpacity={0} />
          </linearGradient>
        </defs>
        <XAxis dataKey="time" />
        <YAxis />
        <Area
          type="monotone"
          dataKey={(obj) => (obj["received"] / obj["transmitted"]) * 100}
          stroke="#EEEEEE"
          strokeOpacity={0.8}
          fill="url(#colorUv)"
        />
      </AreaChart>
    </ResponsiveContainer>
  );
}

import { readFile } from "node:fs/promises";
import { PS_DATADIR } from "@/settings";
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
} from "@/components/table";
import { PingChart } from "@/app/[ip]/[date]/PingChart";
import { NavBar } from "@/components/NavBar";

export const dynamic = "force-dynamic";

export interface ICSVData {
  headers: string[];
  rows: Record<string, string>[];
}

const SEP = ",";

function csvLoader(data: string): ICSVData {
  const [headerLine, ...dataLines] = data.split("\n");

  const headers = headerLine.split(SEP);
  const rows = dataLines.reduce<Record<string, string>[]>((acc, cur) => {
    const values = cur.split(SEP);
    const row = Object.fromEntries(
      headers.map((header, idx) => [header, values[idx]]),
    );
    acc.push(row);
    return acc;
  }, []);
  return {
    headers,
    rows,
  };
}

async function loadData(ip: string, date: string): Promise<IRes<ICSVData>> {
  const fileName = `${PS_DATADIR}/${ip}/${date}.csv`;
  try {
    const rawData = await readFile(fileName, {
      encoding: "utf-8",
    });
    return { data: csvLoader(rawData), error: null };
  } catch (e) {
    return { data: null, error: e?.toString() ?? "unknown error" };
  }
}

export default async function Page({
  params: { ip, date },
}: {
  params: { ip: string; date: string };
}) {
  const csvData = await loadData(ip, date);
  if (csvData.error != null) {
    return <div>{csvData.error}</div>;
  }
  return (
    <div className="flex flex-col items-center">
      <NavBar href={`/${ip}`} />
      <main className="flex flex-col h-screen overflow-clip pt-16 gap-5 max-w-screen-md">
        <PingChart rows={csvData.data.rows} />
        <div className="overflow-auto">
          <Table dense grid striped className="overflow-x-clip">
            <TableHead className="sticky top-0 bg-neutral-900 z-20">
              <TableRow>
                {csvData.data.headers.map((header, key) => (
                  <TableHeader key={key}>{header}</TableHeader>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {csvData.data.rows.map((row, key) => (
                <TableRow key={key}>
                  {Object.values(row).map((cell, key) => (
                    <TableCell key={key}>{cell}</TableCell>
                  ))}
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </div>
      </main>
    </div>
  );
}

import { readdir } from "node:fs/promises";
import { Dirent } from "node:fs";
import { Link } from "@/components/link";
import { PS_DATADIR } from "@/settings";

export const dynamic = "force-dynamic";

async function getDestinations(): Promise<IRes<Dirent[]>> {
  try {
    const d = await readdir(PS_DATADIR, {
      encoding: "utf-8",
      recursive: false,
      withFileTypes: true,
    });
    return { data: d.filter((e) => e.isDirectory()), error: null };
  } catch (e) {
    return { data: null, error: e?.toString() ?? "unknown error" };
  }
}

export default async function Home() {
  const destinations = await getDestinations();
  return (
    <main className="flex min-h-screen flex-col items-center justify-between p-24">
      {destinations.error != null ? (
        <div>{destinations.error}</div>
      ) : (
        <ul>
          {destinations.data.map((dest) => (
            <li className="m-4" key={dest.name}>
              <Link href={`/${dest.name}`}>{dest.name}</Link>
            </li>
          ))}
        </ul>
      )}
    </main>
  );
}
